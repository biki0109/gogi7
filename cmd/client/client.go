package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi7/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi7Client struct {
	pb.Gogi7Client
}

func NewGogi7Client(address string) *Gogi7Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi7 service", l.Error(err))
	}

	c := pb.NewGogi7Client(conn)

	return &Gogi7Client{c}
}
